var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch'), 
    sass = require('gulp-sass'),
    jshint = require('gulp-jshint'),
    concatcss = require('gulp-concat-css'),
    minifycss = require('gulp-minify-css');
        
var filesToMove = [
   './bower_components/blocss/_defaults.scss',
   './bower_components/blocss/base/**/*.*'
];


gulp.task('move-blocss', function () {
   gulp.src(filesToMove, { base: './bower_components/' })
       .pipe(gulp.dest('assets/scss'));
});


gulp.task('minify-js', function () {
   gulp.src('assets/js/**/*.js')
      .pipe(uglify())
      .pipe(gulp.dest('dist/js'));
});

gulp.task('concat-css', function () {
  gulp.src('assets/**/*.css')
    .pipe(concatcss("bundle.css"))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('compile-sass', function () {
   gulp.src('assets/scss/**/*.*')
   	   .pipe(sass())
   	   .pipe(gulp.dest('dist/css'));
});

gulp.task('lint-assets', function () {
   gulp.src('assets/js/**/*.js')
   	 .pipe(jshint())
   	 .pipe(jshint.reporter('default'));
});

gulp.task('lint-app', function () {
   gulp.src('app/**/*.js')
       .pipe(jshint())
       .pipe(jshint.reporter('default'));
});

gulp.task('watch', function () {
   gulp.watch('assets/js/**/*.js', ['lint-assets', 'minify']);
   gulp.watch('assets/scss/**/*.scss', ['compile-sass']);
   gulp.watch('app/**/*.js', ['lint-app']);
});

gulp.task('minify-css', function() {
  gulp.src('assets/css/**/*.css')
    .pipe(minifycss({keepBreaks:false}))
    .pipe(gulp.dest('dist/css'))
});


gulp.task('default', ['compile-sass', 'lint-assets', 'lint-app', 'minify-js', 'minify-css', 'watch']);